import { ethers } from "ethers";
import "./App.css";

const AddBook = ({ state }) => {
  const addBook = async (event) => {
    event.preventDefault();

    const { contract, provider } = state;
    const name = String(document.querySelector("#name").value);
    const year = parseInt(document.querySelector("#year").value);
    const author = String(document.querySelector("#author").value);
    const description = String(document.querySelector("#description").value);
    const isComplete = document.querySelector("#isComplete").value === "true";

    // Basic validations
    if (!name || !year || !author) {
      alert("Please fill in all required fields.");
      return;
    }

    // Check if year is a valid number
    if (isNaN(year) || year < 0) {
      alert("Invalid year value.");
      return;
    }

    // Regular expression to match only alphabetic characters
    const alphabeticRegex = /^[a-zA-Z\s]*$/;

    // Additional validation: Check if name and author contain only alphabetic characters
    if (!alphabeticRegex.test(name) || !alphabeticRegex.test(author)) {
      alert("Name and author must contain only alphabetic characters.");
      return;
    }

    try {
      const estimatedGas = await contract.estimateGas.addBook(
        name,
        year,
        author,
        description,
        isComplete
      );

      // Send the transaction with the estimated gas limit
      const transaction = await contract.addBook(
        name,
        year,
        author,
        description,
        isComplete,
        {
          gasLimit: estimatedGas.mul(120).div(100), // Add some buffer to the estimated gas limit
        }
      );

      console.log("Waiting for transaction...");
      await transaction.wait();
      alert("Transaction is Successful!");
      console.log("Transaction done!");
      window.location.reload();
    } catch (error) {
      console.error("Error adding book:", error);
      alert("Error adding book. Please try again later.");
    }
  };

  return (
    <div className="add-book-container">
      <form onSubmit={addBook}>
        <div className="form-group">
          <label htmlFor="name">Book Name:</label>
          <input id="name" type="text" />
        </div>
        <div className="form-group">
          <label htmlFor="year">Publication Year:</label>
          <input id="year" type="number" />
        </div>
        <div className="form-group">
          <label htmlFor="author">Author:</label>
          <input id="author" type="text" />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description:</label>{" "}
          <textarea id="description"></textarea>{" "}
        </div>
        <div className="form-group">
          <label htmlFor="isComplete">Complete Reading?:</label>
          <select id="isComplete">
            <option value="true">Yes</option>
            <option value="false">No</option>
          </select>
        </div>
        <button className="add-button" type="submit">
          Add to List
        </button>
      </form>
    </div>
  );
};

export default AddBook;

// import { ethers } from "ethers";
// import "./App.css";

// const AddBook = ({ state }) => {
//   const addBook = async (event) => {
//     event.preventDefault();

//     const { contract } = state;
//     const name = document.querySelector("#name").value;
//     const year = parseInt(document.querySelector("#year").value);
//     const author = String(document.querySelector("#author").value);
//     const isComplete = document.querySelector("#isComplete").value === "true";

//     try {
//       const transaction = await contract.addBook(
//         name,
//         year,
//         author,
//         isComplete
//       );
//       console.log("Waiting for transaction...");
//       await transaction.wait();
//       alert("Transaction is Successful!");
//       console.log("Transaction done!");
//       window.location.reload();
//     } catch (error) {
//       console.error("Error adding book:", error);
//     }
//   };

//   return (
//     <div className="add-book-container">
//       <form onSubmit={addBook}>
//         <div className="form-group">
//           <label htmlFor="name">Book Name:</label>
//           <input id="name" type="text" />
//         </div>
//         <div className="form-group">
//           <label htmlFor="year">Publication Year:</label>
//           <input id="year" type="number" />
//         </div>
//         <div className="form-group">
//           <label htmlFor="author">Author:</label>
//           <input id="author" type="text" />
//         </div>
//         <div className="form-group">
//           <label htmlFor="isComplete">Complete Reading?:</label>
//           <select id="isComplete">
//             <option value="true">Yes</option>
//             <option value="false">No</option>
//           </select>
//         </div>
//         <button className="add-button">Add to List</button>
//       </form>
//     </div>
//   );
// };

// export default AddBook;
