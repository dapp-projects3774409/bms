import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div>
      <ul style={{ listStyleType: "none", padding: 0 }}>
        <li>
          <Link to="/AddBook">
            <button
              style={{
                backgroundColor: "green",
                color: "white",
                border: "none",
                padding: "10px 20px",
                borderRadius: "5px",
                marginLeft: "15px",
              }}
            >
              Add Book
            </button>
          </Link>
          <Link to="/">
            <button
              style={{
                backgroundColor: "green",
                color: "white",
                border: "none",
                padding: "10px 20px",
                borderRadius: "5px",
                marginLeft: "1000px",
              }}
            >
              Book List
            </button>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default Navbar;
