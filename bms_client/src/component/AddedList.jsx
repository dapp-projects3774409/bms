import React, { useEffect, useState } from "react";
import "./AddBook.css"; // Import your CSS file

const AddedList = ({ state }) => {
  const [bookList, setBookList] = useState([]);
  const [booksRead, setBooksRead] = useState(0); // Initialize counter
  const [isRadioChecked, setIsRadioChecked] = useState("list"); // State for radio button
  const [previewDescription, setPreviewDescription] = useState(""); // State for preview description
  const [showPreview, setShowPreview] = useState(false); // State to control the visibility of the preview popup
  const [editBookData, setEditBookData] = useState(null); // State for holding edited book data
  console.log(editBookData);
  const [showEditPopup, setShowEditPopup] = useState(false); // State to control the visibility of the edit popup form

  const { contract } = state;

  useEffect(() => {
    const fetchBookLists = async () => {
      try {
        let fetchedBooks = [];
        if (isRadioChecked === "list") {
          const finishedBooks = await contract.getBookList(true);
          const unfinishedBooks = await contract.getBookList(false);
          fetchedBooks = [...finishedBooks, ...unfinishedBooks];
        } else if (isRadioChecked === "finished") {
          fetchedBooks = await contract.getBookList(true);
        } else if (isRadioChecked === "unfinished") {
          fetchedBooks = await contract.getBookList(false);
        }
        setBookList(fetchedBooks);
        // Count the number of books marked as read
        const numRead = fetchedBooks.filter((item) => item[4]).length;
        setBooksRead(numRead);
      } catch (error) {
        console.error("Error fetching book lists:", error);
      }
    };
    contract && fetchBookLists();
  }, [contract, isRadioChecked]); // Added isRadioChecked as a dependency

  // Function to handle radio button change
  const handleRadioChange = (event) => {
    setIsRadioChecked(event.target.value); // Set the state to the value of the selected radio button
  };

  // // Function to handle editing a book
  // const handleEditBook = async (bookId) => {
  //   try {
  //     // Call the contract function to get the book details
  //     // Convert the BigNumber bookId to an integer
  //     const intBookId = bookId.toNumber();
  //     console.log(intBookId);
  //     const bookDetails = await contract.getBookDetails(intBookId);
  //     const [_, bookTitle, bookAuthor, bookYear, bookDescription, isCompleted] =
  //       bookDetails;
  //     console.log(bookDetails);
  //     // Populate the editBookData state with the retrieved details
  //     setEditBookData({
  //       title: bookTitle,
  //       author: bookAuthor,
  //       year: parseInt(bookYear._hex, 16),
  //       description: bookDescription,
  //       isCompleted: isCompleted,
  //     });
  //     setShowEditPopup(true);
  //   } catch (error) {
  //     console.error("Error fetching book details for editing:", error);
  //   }
  // };

  // // Function to handle saving edits
  // const handleSaveEdit = async () => {
  //   const { bookId, title, author, year, description, isCompleted } =
  //     editBookData;
  //   // Convert the BigNumber bookId to an integer
  //   const intBookId = bookId.toNumber();
  //   console.log(intBookId);
  //   try {
  //     await contract.editBook(
  //       intBookId,
  //       title,
  //       year,
  //       author,
  //       description,
  //       isCompleted
  //     );
  //     setShowEditPopup(false);
  //     alert("Edition successful");
  //   } catch (error) {
  //     console.error("Error editing book:", error);
  //   }
  // };

  // // Function to handle canceling edits
  // const handleCancelEdit = () => {
  //   setShowEditPopup(false);
  // };

  // Function to handle deleting a book
  const handleDeleteBook = async (bookId) => {
    // Convert the BigNumber bookId to an integer
    const intBookId = bookId.toNumber();
    console.log(intBookId);

    // Implement the logic to handle deleting a book
    // Call the corresponding contract function
    try {
      // Call the contract function to delete the book
      await contract.deleteBook(intBookId);
      // Refetch the book lists after deletion
      alert("Deletion successful");
    } catch (error) {
      console.error("Error deleting book:", error);
    }
  };

  // Function to handle previewing a book
  const handlePreviewBook = async (bookId) => {
    try {
      // Call the contract function to get the description
      const description = await contract.getDescription(bookId);
      setPreviewDescription(description);
      // Show the preview popup
      setShowPreview(true);
    } catch (error) {
      console.error("Error fetching book description:", error);
    }
  };

  // Function to close the preview popup
  const handleClosePreview = () => {
    // Clear the preview description and hide the preview popup
    setPreviewDescription("");
    setShowPreview(false);
  };

  return (
    <div className="container-fluid">
      <h2
        style={{
          textAlign: "center",
          marginTop: "20px",
          color: "black",
        }}
      >
        List of Books Added!
      </h2>
      <div style={{ textAlign: "center" }}>
        <p>
          <b>Total books read:</b> {booksRead}
        </p>
        <div className="radioContainer">
          <label>
            <input
              type="radio"
              name="bookStatus"
              value="list"
              checked={isRadioChecked === "list"}
              onChange={handleRadioChange}
            />
            List of Book
          </label>
          <label>
            <input
              type="radio"
              name="bookStatus"
              value="finished"
              checked={isRadioChecked === "finished"}
              onChange={handleRadioChange}
            />
            Finished Book
          </label>
          <label>
            <input
              type="radio"
              name="bookStatus"
              value="unfinished"
              checked={isRadioChecked === "unfinished"}
              onChange={handleRadioChange}
            />
            Unfinished Book
          </label>
        </div>
      </div>
      {bookList.length > 0 ? (
        <table className="book-table">
          <thead>
            <tr>
              <th
                style={{
                  backgroundColor: "#3e3c3c",
                  color: "white",
                  textAlign: "center",
                }}
              >
                Title
              </th>
              <th
                style={{
                  backgroundColor: "#3e3c3c",
                  color: "white",
                  textAlign: "center",
                }}
              >
                Author
              </th>
              <th
                style={{
                  backgroundColor: "#3e3c3c",
                  color: "white",
                  textAlign: "center",
                }}
              >
                Publication Year
              </th>
              <th
                style={{
                  backgroundColor: "#3e3c3c",
                  color: "white",
                  textAlign: "center",
                }}
              >
                isCompleted
              </th>
              <th
                style={{
                  backgroundColor: "#3e3c3c",
                  color: "white",
                  textAlign: "center",
                }}
              >
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {bookList.map((item, index) => (
              <tr key={index}>
                <td style={{ textAlign: "center" }}>{item[1]}</td>
                <td>{item[3]}</td>
                <td>{parseInt(item[2]._hex, 16)}</td>
                <td>{item[4] ? "Yes" : "No"}</td>
                <td>
                  {/* <button
                    onClick={() =>
                      handleEditBook(
                        item.id,
                        item[1],
                        item[3],
                        parseInt(item[2]._hex, 16)
                      )
                    }
                  >
                    Edit
                  </button> */}
                  <button onClick={() => handleDeleteBook(item.id)}>
                    Delete
                  </button>
                  <button onClick={() => handlePreviewBook(item.id)}>
                    Preview
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <p>No books added yet.</p>
      )}
      Preview Popup
      {showPreview && (
        <div className="preview-popup">
          <div className="preview-content">
            <span className="close" onClick={handleClosePreview}>
              &times;
            </span>
            <h3>Book Preview</h3>
            <p>{previewDescription}</p>
          </div>
        </div>
      )}
      {/* Edit Popup Form
      {showEditPopup && (
        <div className="edit-popup">
          <div className="edit-content">
            <h3>Edit Book</h3>
            <label>Title:</label>
            <input
              type="text"
              value={editBookData.title}
              onChange={(e) =>
                setEditBookData({ ...editBookData, title: e.target.value })
              }
            />
            <label>Author:</label>
            <input
              type="text"
              value={editBookData.author}
              onChange={(e) =>
                setEditBookData({ ...editBookData, author: e.target.value })
              }
            />
            <label>Publication Year:</label>
            <input
              type="number"
              value={editBookData.year}
              onChange={(e) =>
                setEditBookData({ ...editBookData, year: e.target.value })
              }
            />
            <label>Description:</label>
            <textarea
              value={editBookData.bookDescription}
              onChange={(e) =>
                setEditBookData({
                  ...editBookData,
                  description: e.target.value,
                })
              }
            />
            <label>
              Completed:
              <input
                type="checkbox"
                checked={editBookData.isCompleted}
                onChange={(e) =>
                  setEditBookData({
                    ...editBookData,
                    isCompleted: e.target.checked,
                  })
                }
              />
            </label>
            <button onClick={handleSaveEdit}>Save</button>
            <button onClick={handleCancelEdit}>Cancel</button>
          </div>
        </div>
      )} */}
    </div>
  );
};

export default AddedList;
