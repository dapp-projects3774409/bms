const ethers = require("ethers");
require("dotenv").config();

const sepolia_url = process.env.sepolia_url;
const private_key = process.env.private_key;
const contractAddress = process.env.contractAddress;
console.log(sepolia_url)
const provider = new ethers.providers.JsonRpcProvider(sepolia_url);
const signer = new ethers.Wallet(private_key, provider);
const { abi } = require("./artifacts/contracts/Booklist.sol/Booklist.json");
const contractInstance = new ethers.Contract(contractAddress, abi, signer);

const express = require("express")
const app = express();
app.use(express.json());

app.post("/books/addBooks", async (req, res) => {
    try {
        const { name, year, author, isComplete } = req.body;
        const tx = await contractInstance.addBook(name, year, author, isComplete);
        await tx.wait;
        res.json({ success: true })
    }
    catch (error) {
        res.status(500).send(error.message);
    }
})
app.get("/books/getAllBooks", async (req, res) => {
    try {
        const allBooks = await contractInstance.getAllBooks();
        // const unfinishedBooks = await contractInstance.getBookList(false);
        // const allBooks = [...finishedBooks, ...unfinishedBooks];

        const books = allBooks.map(book => ({
            id: parseInt(book.id),
            name: book.name,
            year: parseInt(book.year),
            author: book.author,
            isComplete: book.isComplete
        }))
        console.log(books)
        res.send(books);
    }
    catch (error) {
        res.status(500).send(error.message);
    }
})

const port = 3000;
app.listen(port, () => {
    console.log("API server is listening on port 3000")
})