//SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.7.6 <0.9.0;

//custom errors to save gas 
error BookListEmpty();
error InvalidBookId();
error NotBookOwner();
contract Booklist{
    struct Book{
        uint id;
        string name;
        uint year;
        string author;
        string description; // New field for book description or preview
        bool isCompleted;
    }

    Book[] private bookList;
    mapping(uint256 => address) bookToOwner;

    event AddBook(address bookRecipient, uint bookId);
    event EditBook(uint bookId);
    event DeleteBook(uint bookId);
    event SetFinished(uint bookId, bool bookFinished);

    function addBook(string memory name, uint year, string memory author, string memory description, bool isCompleted) external {
        require(bytes(name).length > 0 && bytes(author).length > 0, "Name and author cannot be empty");
        
        uint bookId = bookList.length;
        bookList.push(Book(bookId, name, year, author, description, isCompleted));
        bookToOwner[bookId] = msg.sender;
        emit AddBook(msg.sender, bookId);
    }

    function editBook(uint _bookId, string memory name, uint year, string memory author, string memory description, bool isCompleted) external  onlyOwner(_bookId) {
        
        bool bookFound = false;
        // Shift elements to fill the gap
        for (uint i = 0; i < bookList.length; i++) {
            if (bookList[i].id == _bookId){
                bookFound = true;
                bookList[i].name = name;
                bookList[i].year = year;
                bookList[i].author = author;
                bookList[i].description = description;
                bookList[i].isCompleted = isCompleted;
            }
        }
        require(bookFound, "Book does not exist");
        
        emit EditBook(_bookId);
    }

    function deleteBook(uint _bookId) external onlyOwner(_bookId) {
        bool bookFound = false;
        // Shift elements to fill the gap
        for (uint i = 0; i < bookList.length; i++) {
            if (bookList[i].id == _bookId){
                bookFound = true;
                bookList[i] = bookList[bookList.length - 1];
                bookList.pop();
            }
        }
        require(bookFound, "Book does not exist");

        emit DeleteBook(_bookId);
    }

  
    function getBookDetails(uint _bookId) public view returns (string memory title, string memory _author, uint256 _year, string memory _description, bool _isComplete) {
         bool bookFound = false;
        // Shift elements to fill the gap
        for (uint i = 0; i < bookList.length; i++) {
            if (bookList[i].id == _bookId){
                bookFound = true;
                 return (bookList[i].name, bookList[i].author, bookList[i].year, bookList[i].description, bookList[i].isCompleted);
            }
        }
        require(bookFound, "Book does not exist");
    }

    function setCompleted(uint _bookId) external bookExists(_bookId) onlyOwner(_bookId) {
        require(!bookList[_bookId].isCompleted, "Book is already marked as completed");
        bookList[_bookId].isCompleted = true;
        emit SetFinished(_bookId, true);
    }

    modifier onlyOwner(uint _bookId){
        require(msg.sender == bookToOwner[_bookId],"NotBookOwner()");
        _;
    }

    modifier bookExists(uint _bookId){
        require(_bookId < bookList.length, "Book does not exist");
        _;
    }



     function getUnfinishedBook() external view returns (Book[] memory){
        return getBookList(false);
    }
    function getFinishedBook() external view returns (Book[] memory){
        return getBookList(true);
    }

    function getDescription(uint _bookId) external view bookExists(_bookId) returns (string memory) {
        return bookList[_bookId].description;
    }

    function getAllBooks() external view returns (Book[] memory) {
        return bookList;
    }

   function getBookList(bool finished) public view returns (Book[] memory){
        uint count = 0;
        for (uint index = 0; index < bookList.length; index++){
            if (bookToOwner[index] == msg.sender && bookList[index].isCompleted == finished){
                count++;
            }
        }
        Book[] memory temp = new Book[](count);
        uint tempIndex = 0;
        for (uint index = 0; index < bookList.length; index++){
            if (bookToOwner[index]==msg.sender && bookList[index].isCompleted == finished){
                temp[tempIndex] = bookList[index];
                tempIndex++;
            }
        }
        return temp;
    }
}
